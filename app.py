import pandas as pd
import numpy as np
import joblib
import streamlit as st
import os
import pandas_profiling
from streamlit_pandas_profiling import st_profile_report
from pycaret.classification import setup, compare_models, pull

# Setting the web app
with st.sidebar:
    MakeChoice = st.radio("Nav", ["Main page","Dataset Upload", "X", "Y"])

# Setting the web main page
if MakeChoice == "Main page":    
    st.title("Titanic prediction App")

# Checking if DF is available
if os.path.exists("data/src_data.csv"):
    df = pd.read_csv("data/src_data.csv", index_col=None)

# Uploading the dataset
if MakeChoice == "Dataset Upload":
    st.title("Dataset Upload")
    file = st.file_uploader("Let's upload your dataset :")
    if file:
        # loading csv file from user
        df = pd.read_csv(file, index_col=None)
        
        # saving the dataset on our own
        df.to_csv("data/src_data.csv", index=None)

        st.dataframe(df)
        pass

# Auto data xpltion ?
if MakeChoice == "X":
    st.title("Analyzing the dataset")
    report = df.profile_report()
    st_profile_report(report)
    pass

# Auto ML stuffs ?
if MakeChoice == "Y":
    st.title("ML Stuffffffff")
    target = st.selectbox("Select the target", df.columns)
    setup(df, target=target, silent=True)
    setup_df = pull()
    st.dataframe(setup_df)
    best_model = compare_models()
    compare_df = pull()
    st.dataframe(compare_df)
    best_model

    pass
